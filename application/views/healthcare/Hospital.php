<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Hospital</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button {
				-webkit-appearance: none;
		</style>
	</head>
	<body>
		<div class="page-wrapper">
			<div class="container-fluid"></div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h4>HOSPITAL</h4>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-12 col-xs-12">
											<div class="form-wrap">



													  <form class="form-horizontal" action="<?php echo base_url('Healthcare/hospital_model')?>" method="POST">
														<br>
															<div class="form-group">
															<label class=" col-sm-4" for="Hospital Name">HOSPITAL NAME:</label>
															<div class="col-sm-6">
															  <input type="text" class="form-control" placeholder="Enter Hospital Name" name="hname">
															</div>
														</div>
														<div class="form-group">
															<label class=" col-sm-4" for="Hospital Address">HOSPITAL ADDRESS:</label>
															<div class="col-sm-6">
															  <input type="text" class="form-control" placeholder="Enter Hospital Address" name="haddress">
															</div>
														</div>
														<div class="form-group">
															<label class=" col-sm-4" for="Hospital Contact">HOSPITAL CONTACT:</label>
															<div class="col-sm-6">
															  <input type="number" class="form-control" placeholder="Enter Hospital Contact" name="contact" maxLength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
															</div>
														</div>
														
														<input type="Submit" class="btn btn-default btn-success" name="submit">
														<input type="Reset" class="btn btn-default">
														</form>
										</div>							
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>											


</body>
</html>
