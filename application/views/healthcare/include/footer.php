<?php
$this->load->helper("url");
?>
<html>
	<body>
	
	<!-- Footer -->
	
			<footer class="footer container-fluid pl-30 pr-30">
				<div class="row">
					<div class="col-sm-12">
						<p>2017 &copy; DCNTV. All rights reserved</p>
					</div>
				</div>
			</footer>
			<!-- /Footer -->
		
		
	<!-- JavaScript -->
	
    <!-- jQuery -->
    <script src="<?php echo base_url('vendors/bower_components/jquery/dist/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    
	<!-- Data table JavaScript -->
	<script src="<?php echo base_url('vendors/bower_components/datatables/media/js/jquery.dataTables.min.js'); ?>"></script>
	
	<!-- Slimscroll JavaScript -->
	<script src="<?php echo base_url('full-width-light/dist/js/jquery.slimscroll.js'); ?>"></script>
	
	<!-- simpleWeather JavaScript -->
	<script src="<?php echo base_url('vendors/bower_components/moment/min/moment.min.js'); ?>"></script>
	<script src="<?php echo base_url('vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js'); ?>"></script>
	<script src="<?php echo base_url('full-width-light/dist/js/simpleweather-data.js'); ?>"></script>
	
	<!-- Progressbar Animation JavaScript -->
	<script src="<?php echo base_url('vendors/bower_components/waypoints/lib/jquery.waypoints.min.js'); ?>"></script>
	<script src="<?php echo base_url('vendors/bower_components/jquery.counterup/jquery.counterup.min.js'); ?>"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="<?php echo base_url('full-width-light/dist/js/dropdown-bootstrap-extended.js'); ?>"></script>
	
	<!-- Sparkline JavaScript -->
	<script src="<?php echo base_url('vendors/jquery.sparkline/dist/jquery.sparkline.min.js'); ?>"></script>
	
	<!-- Owl JavaScript -->
	<script src="<?php echo base_url('vendors/bower_components/owl.carousel/dist/owl.carousel.min.js'); ?>"></script>
	
	<!-- ChartJS JavaScript -->
	<script src="<?php echo base_url('vendors/chart.js/Chart.min.js'); ?>"></script>
	
	<!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url('vendors/bower_components/raphael/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('vendors/bower_components/morris.js/morris.min.js'); ?>"></script>
    <script src="<?php echo base_url('vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js'); ?>"></script>
	
	<!-- Switchery JavaScript -->
	<script src="<?php echo base_url('vendors/bower_components/switchery/dist/switchery.min.js'); ?>"></script>
	
	<!-- Init JavaScript -->
	<script src="<?php echo base_url('full-width-light/dist/js/init.js'); ?>"></script>
	<script src="<?php echo base_url('full-width-light/dist/js/dashboard-data.js'); ?>"></script>
</body>


<!-- Mirrored from hencework.com/theme/doodle/full-width-light/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 03 Jan 2018 11:15:13 GMT -->
</html>
