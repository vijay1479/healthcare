<html>
	</head>
	</head>
	<body>
	
	
		<!-- Main Content -->
			<div class="page-wrapper">
				<div class="container-fluid">

						<!-- Breadcrumb -->
						<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
							<ol class="breadcrumb">
								<li><a href="index-2.html">Dashboard</a></li>
								<li><a href="#"><span>form</span></a></li>
								<li class="active"><span>form-layout</span></li>
							</ol>
						</div>
						<!-- /Breadcrumb -->
					
				</div>
					<!-- /Title -->
				<!-- Row -->
				
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">Register</h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-wrap">
													<form action="<?php echo site_url('User_con/user_insert'); ?>" method="POST" class="form-horizontal">
														<div class="form-body">
															<h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Person's Info</h6>
															<hr class="light-grey-hr"/>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label col-md-3">First Name</label>
																		<div class="col-md-9">
																			<input type="text" name="fname" class="form-control">
																			
																		</div>
																	</div>
																</div>
																<!--/span-->
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label col-md-3">Last Name</label>
																		<div class="col-md-9">
																			<input type="text" name="lname" class="form-control">
										 
																		</div>
																	</div>
																</div>
																<!--/span-->
															</div>
															<!-- /Row -->
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label col-md-3">Username</label>
																		<div class="col-md-9">
																			<input type="text" name="uname" class="form-control">
																		</div>
																	</div>
																</div>
																
																<!--/span-->
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label col-md-3">Contact</label>
																		<div class="col-md-9">
																			<input type="number" name="contact" class="form-control">
																		</div>
																	</div>
																</div>
																<!--/span-->
															</div>
															<!-- /Row -->
															<div class="row">
																	<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label col-md-3">Email</label>
																		<div class="col-md-9">
																			<input type="email" name="email" class="form-control">
										 
																		</div>
																	</div>
																</div>
																
																<!--/span-->
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label col-md-3">Designation</label>
																		<div class="col-md-9">
																			<select class="form-control" name="designation">
																				<option value="0">--Select--</option>
																				<?php 
																					foreach($designation as $row)
																					{
																						echo '<option value="'.$row->USER_ID.'">'.$row->USER_ROLE.'</option>';
																					}
																				?>
																			</select>
										 
																		</div>
																	</div>
																</div>
																<!--/span-->
															</div>
															<!-- /Row -->
														</div>
														<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="control-label col-md-3">Password</label>
																		<div class="col-md-9">
																			<input type="password" name="password" class="form-control">
																			
																		</div>
																	</div>
																</div>
																<!--/span-->
															</div>
														<div class="form-actions mt-10">
															<div class="row">
																<div class="col-md-6">
																	<div class="row">
																		<div class="col-md-offset-3 col-md-9">
																			<input type="Submit" name="submit" class="btn btn-success  mr-10">
																			<button type="button" class="btn btn-default">Cancel</button>
																		</div>
																	</div>
																</div>
																<div class="col-md-6"> </div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->
			
	</body>
</html>