<!DOCTYPE html>
<html lang="en">
<head>
  <title>Subscriber</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style>
		input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
	</style>
</head>
<body>

<div class="container">
  <center><h2>Subscriber Registration</h2></center>
  <?php $this->load->helper('url'); ?>
  <form class="form-horizontal" action="<?php echo base_url('test/insert_details')?>" method="post" enctype="multipart/form-data">
	<br><br>
	<div class="form-group">
		<label class=" col-sm-4" for="Registration Number">REGISTRATION NUMBER:</label>
		<div class="col-sm-6">
		  <input type="number" class="form-control" placeholder="Enter Registration Number" name="reg_num" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="First Name">FIRST NAME:</label>
		<div class="col-sm-6">
		  <input type="text" class="form-control" placeholder="Enter First Name" name="fname" required>
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="Last Name">LAST NAME:</label>
		<div class="col-sm-6">
		  <input type="text" class="form-control" placeholder="Enter Last Name" name="lname" required>
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="Age">AGE:</label>
		<div class="col-sm-6">
		  <input type="number" class="form-control" placeholder="Enter Age" name="age" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="DOB">DOB:</label>
		<div class="col-sm-6">
		  <input type="date" class="form-control" placeholder="Enter DOB" name="dob" required>
		</div>
	</div>
	
	<div class="form-group">
		<label class=" col-sm-4" for="bgroup">BLOOD GROUP:</label>
		<div class="col-sm-6">
		  <select class="form-control" name="blood">
			<option value="0">--Select--</option>
			<?php 
				foreach($blood as $row)
				{
					echo '<option value="'.$row->BLOOD_GROUP_ID.'">'.$row->BLOOD_NAME.'</option>';
				}
			?>
		</select>
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="disease">Disease:</label>
		<div class="col-sm-6">
		  <select class="form-control" name="disease">
			<option value="0">--Select--</option>
			<?php 
				foreach($disease as $row)
				{
					echo '<option value="'.$row->DISEASE_ID.'">'.$row->DISEASE_NAME.'</option>';
				}
			?>
		</select>
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="Nominee">NOMINEE:</label>
		<div class="col-sm-6">
		  <input type="text" class="form-control" placeholder="Enter Nominee" name="nominee">
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="Members">MEMBERS:</label>
		<div class="col-sm-6">
		  <input type="number" class="form-control" placeholder="Enter Members" name="member" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="Profession">PROFESSION:</label>
		<div class="col-sm-6">
		 <select class="form-control" name="profession">
			<option value="0">--Select--</option>
			<?php 
				foreach($profession as $row)
				{
					echo '<option value="'.$row->PROFESSION_ID.'">'.$row->PROFESSION_NAME.'</option>';
				}
			?>
		</select>
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="Contact">CONTACT:</label>
		<div class="col-sm-6">
		  <input type="number" class="form-control" placeholder="Enter Contact" name="contact" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="Email">EMAIL:</label>
		<div class="col-sm-6">
		  <input type="email" class="form-control" placeholder="Enter Email" name="email">
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="Address">ADDRESS:</label>
		<div class="col-sm-6">
		  <input type="text" class="form-control" placeholder="Enter Address" name="address">
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="image">IMAGE:</label>
		<div class="col-sm-6">
		  <input type="file" class="form-control" name="profile" id="image">
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="Aadhar">AADHAR:</label>
		<div class="col-sm-6">
		  <input type="text" class="form-control" placeholder="Enter Aadhar" name="aadhar" maxlength="12" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
		</div>
	</div>
	<div class="form-group">
		<label class=" col-sm-4" for="Signature">SIGNATURE:</label>
		<div class="col-sm-6">
		  <input type="file" class="form-control" placeholder="Enter Signature" name="signature" id="signature">
		</div>
	</div>
    <div class="form-group">
		<label class=" col-sm-4" for="Remark">REMARK:</label>
		<div class="col-sm-6">
		  <input type="text" class="form-control" placeholder="Enter Remark" name="remark">
		</div>
	</div> 
       
	<input type="Submit" class="btn btn-default btn-success" name="submit">
	<input type="Reset" class="btn btn-default">
	
  </form>
</div>

</body>
</html>