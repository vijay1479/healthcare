<?php

defined('BASEPATH') OR exit('No direct script access allowed');

 class Subscriber_model extends CI_Model 
{ 
	public function select_disease()   
	{   
		$this->db->select('DISEASE_NAME,DISEASE_ID');
		$this->db->from('disease');
		$query = $this->db->get();
		return $query->result();
	}
	public function select_blood()   
	{   
		$this->db->select('BLOOD_GROUP_ID,BLOOD_NAME');
		$this->db->from('blood_group');
		$query = $this->db->get();
		return $query->result();
	}
	public function select_profession()   
	{   
		$this->db->select('PROFESSION_ID,PROFESSION_NAME');
		$this->db->from('profession');
		$query = $this->db->get();
		return $query->result();
	}
}
?>