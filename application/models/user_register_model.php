 <?php

defined('BASEPATH') OR exit('No direct script access allowed');

	class User_register_model extends CI_controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}
		public function get_designation()
		{
			$q=$this->db->get("user_designation");
			return $q->result();
		}
	}