<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller 
{
	public function index()
	{
		$this->load->view('login');
	}
	
	public function registration()
	{
		$this->load->model("Subscriber_model");
		$data["disease"]=$this->Subscriber_model->select_disease();
		$this->load->model("Subscriber_model");
		$data["blood"]=$this->Subscriber_model->select_blood();
		$this->load->model("Subscriber_model");
		$data["profession"]=$this->Subscriber_model->select_profession();
		$this->load->view('registration',$data);
	}

	public function insert_details()
	{
		if (isset($_POST["submit"])) 
		{
			//print_r($_POST);
			//return ;
			$this->load->helper('url');
			$profile_name = $_FILES['profile']['name'];
			$profile_tmp = $_FILES['profile']['tmp_name'];
			$profile_store = 'images/profile/'.$profile_name;

			$signature_name = $_FILES['signature']['name'];
			$signature_tmp = $_FILES['signature']['tmp_name'];
			$signature_store = 'images/signature/'.$signature_name;

			move_uploaded_file($profile_tmp,$profile_store);
			move_uploaded_file($signature_tmp,$signature_store);

			$insert_data = array
				(
				'REGISTRATION_NO' => $_POST['reg_num'],
				'FIRST_NAME' => $_POST['fname'],
				'LAST_NAME' => $_POST['lname'] ,
				'AGE' => $_POST['age'],
				'DOB' => $_POST['dob'],
				'BLOOD_GROUP_ID' => $_POST['blood'],
				'DICEASE_ID' => $_POST['disease'],
				'NOMANIEE' => $_POST['nominee'],
				'MEMBERS' => $_POST['member'],
				'PROFESSION_ID' => $_POST['profession'],
				'CONTACT' => $_POST['contact'],
				'EMAIL' => $_POST['email'],
				'ADDRESS' => $_POST['address'],
				'PROFILE_IMAGE' =>$profile_store,
				'ADHAAR' => $_POST['aadhar'],
				'SIGNATURE' => $signature_store,
				'REMARK' => $_POST['remark'] 
				);
			
			$this->load->database();
			$this->db->insert("subscribers",$insert_data);
			echo "<script>alert('record inserted');</script>";
			$this->registration();
		}
	}

	public function image()
	{
		$this->load->view("imageupload");
	}
}
