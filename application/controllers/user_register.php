<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class User_register extends CI_controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->database();
		}

		public function register_form()
		{
			$this->load->model('user_register_model');
			$data['designation']=$this->user_register_model->get_designation();
			$this->load->view('healthcare/register',$data);
		}

		public function insert_data()
		{
			$data = array(
				'FIRST_NAME' => $_POST['fname'] ,
				'LAST_NAME' => $_POST['lname'] ,
				'USER_NAME' => $_POST['uname'] ,
				'CONTACT' => $_POST['contact'] ,
				'EMAIL' => $_POST['email'] ,
				'USER_DESIGNATION_ID' => $_POST['designation'] ,
				'PASSWORD' => $_POST['password']  
				);
			$this->db->insert('user',$data);
		}

		public function login()
		{
			$this->load->view("include/header");
			$this->load->view("include/footer");
		}
	}
?>
