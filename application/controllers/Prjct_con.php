<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prjct_con extends CI_Controller 
{  
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		
	}
	public function register()    
	{
		
		$this->load->view('include/header'); 
		$this->load->view('main/register'); 
		$this->load->view('include/footer');		
	}
	public function insert()
	{
		
		$this->load->database();
		if($this->input->post('submit'))
		{
			unset($_POST['submit']);
			$this->db->insert('user1',$this->input->post());
		}
		redirect('Prjct_con/register');
	}
}

?>