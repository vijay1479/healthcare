<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Healthcare extends CI_Controller 
{  
	public function subscriber()
	{
		$this->load->helper('url');
		$this->load->model('subscriber_model',"disease");
		$data['disease']=$this->disease->select_disease();
		$this->load->model('subscriber_model',"blood");
		$data['blood']=$this->blood->select_blood();
		$this->load->model('subscriber_model',"profession");
		$data['profession']=$this->profession->select_profession();
		$this->load->view('include/header');
		$this->load->view('Subscriber',$data);
		$this->load->view('include/footer');
	}
	public function subscriber_model()
	{
		$this->load->model('Subscriber_model');
		$this->Subscriber_model->insert_subscriber();
	
	}
	public function staff()
	{
		$this->load->helper('url');
		$this->load->view('include/header');
		$this->load->view('Staff');
		$this->load->view('include/footer');
	}
	public function staff_model()
	{
		
		$data=array("FIRST_NAME"=>$this->input->post('fname'),"LAST_NAME"=>$this->input->post('lname'),"CONTACT"=>$this->input->post('contact'));
		$this->load->database();  
		$this->db->insert('staff',$data);
		
	}
	public function hospital()
	{
		$this->load->helper('url');
		$this->load->view('include/header');
		$this->load->view('Hospital');
		$this->load->view('include/footer');
	}
	public function hospital_model()
	{
		
		$data=array("HOSPITAL_NAME"=>$this->input->post('hname'),"HOSPITAL_ADDRESS"=>$this->input->post('haddress'),"HOSPITAL_CONTACT"=>$this->input->post('contact'));
		$this->load->database();  
		$this->db->insert('hospital',$data);
		
	}
}
?>