<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subscriber_con extends CI_Controller 
{  
	public function subscriber()
	{
		$this->load->helper('url');
		$this->load->model('subscriber_model',"disease");
		$data['disease']=$this->disease->select_disease();
		$this->load->model('subscriber_model',"blood");
		$data['blood']=$this->blood->select_blood();
		$this->load->model('subscriber_model',"profession");
		$data['profession']=$this->profession->select_profession();
		$this->load->view('include/header');
		$this->load->view('healthcare/Subscriber',$data);
		$this->load->view('include/footer');
	}
	public function subscriber_model()
	{
		$this->load->model('Subscriber_model');
		$this->Subscriber_model->insert_subscriber();
	
	}
}
?>