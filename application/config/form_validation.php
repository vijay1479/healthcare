<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
        'reg'=>array(
		
						array(
								'field' => 'name',
								'label' => 'name',
								'rules' => 'required|alpha'
						),
					
						array(
								'field' => 'age',
								'label' => 'age',
								'rules' => 'required|numeric|max_length[2]'
						),
						array(
								'field' => 'dob',
								'label' => 'date',
								'rules' => 'required'
						),
						array(
								'field' => 'mobile',
								'label' => 'mobile',
								'rules' => 'required|numeric'
						),
						array(
								'field' => 'email',
								'label' => 'email',
								'rules' => 'required|valid_email'
						),
						array(
								'field' => 'pass',
								'label' => 'Password',
								'rules' => 'required|alpha_numeric'
						),
						array(
								'field' => 'pass1',
								'label' => 'Password',
								'rules' => 'required|matches[pass]'
						)
					),
					
				'login'=>array(
		
							array(
								'field' => 'npass',
								'label' => 'Password',
								'rules' => 'required'
							),
					
							array(
								'field' => 'rpass',
								'label' => 'Password',
								'rules' => 'required|matches["npass"]'
							)
						
					)	
		
						
);

?>