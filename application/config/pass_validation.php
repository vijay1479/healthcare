<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
        'login'=>array(
		
						array(
								'field' => 'opass',
								'label' => 'opass',
								'rules' => 'required'
						),
					
						array(
								'field' => 'npass',
								'label' => 'npass',
								'rules' => 'required'
						),
						array(
								'field' => 'rpass',
								'label' => 'rpass',
								'rules' => 'required|matches['npass']'
						),
						
					)
						
);

?>